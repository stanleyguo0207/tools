Linux工具:

-   性能测试工具:

    **gperftools (originally Google Performance Tools)**

    https://github.com/gperftools/gperftools

-   内存检测工具:

    **Valgrind** (Valgrind is an instrumentation framework for building dynamic analysis tools. )

    https://valgrind.org/

    **AddressSanitizer, ThreadSanitizer, MemorySanitizer**

    https://github.com/google/sanitizers
